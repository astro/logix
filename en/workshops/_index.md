---
title: Workshops
---
### Organising a workshop
 * from Day3, you can hold workshop at card0 village between 12:00 and 23:00, in 1 hr slots, starting at the full hour, check which slots are already used in the list below
 * for other days/times please coordinate with lilafisch
 * feel free to hold card10 workshops at other locations as well
 * for help, ask lilafisch

### Participating
 * there is no sign-up, just turn up
 * check the worksop descriptions to see if you should bring anything/have anything pre-installed on your laptop
 * you might also find more card10 related sessions on the cccamp19 [self organised sessions page](https://events.ccc.de/camp/2019/wiki/Static:Self-organized_Sessions)
 
## Workshops
please keep chronological order

### Environmental and IMU sensor workshop
 * Time: Day 2, 19:00
 * Location: card10 village


