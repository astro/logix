---
title: Developing
---

You can find the repositories for the apps here:

* [git.card10.badge.events.ccc.de/card10/companion-app-android](https://git.card10.badge.events.ccc.de/card10/companion-app-android/)
* [git.card10.badge.events.ccc.de/card10/companion-app-ios](https://git.card10.badge.events.ccc.de/card10/companion-app-ios/)

Please join the channel [#card10companion:asra.gr](https://riot.im/app/#/room/#freenode_#card10companion:matrix.org)

### Android / F-Droid nightly

The GitLab delivere a (unsafe) Android Debug Build of Companion App by a F-Droid repository.
If you are able to create debug logs and create Issues feel free to subscribe this repository.


[![https://git.card10.badge.events.ccc.de/card10/companion-app-android-nightly/raw/master/fdroid/repo](https://git.card10.badge.events.ccc.de/card10/companion-app-android-nightly/raw/master/icon.png)](https://git.card10.badge.events.ccc.de/card10/companion-app-android-nightly/raw/master/fdroid/repo)

### iOS Testflight builds

* not yet available. Meanwhile, please use [Xcode](https://developer.apple.com/xcode/) to install the app on your iOS device.

